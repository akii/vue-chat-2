export default (items, prop1, prop2, val, startsWith = false) => {
	if (!val || val === '') return items

	return items.filter(v => {
		if (startsWith) return formatString(v[prop1]).startsWith(formatString(val)) || formatString(v[prop2]).startsWith(formatString(val))
		return formatString(v[prop1]).includes(formatString(val)) || formatString(v[prop2]).includes(formatString(val))
	})
}

function formatString(string) {
	// console.log(string)
	return string
		.toLowerCase()
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, '')
}
